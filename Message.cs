﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Games
{
    public static class Message
    {
        public const string WELCOME = "Welcome to the Tic-Tac-Toe game";

        public const string PRESS_THE_SPACE_BAR_TO_CONTINUE = "Press the space bar to continue";

        public const string CHOICE_PLAYER_1_WILL_PLAY_FOR = """
            Please select which sign Player 1 will play for? 
            
            Key X - player 1 plays for X
            Key O - player 1 plays for O
            """;

        public const string CHOICE_A_GAME_AGAINST_THE_COMPUTER = """
            Please select whether you will play against a computer? 
            
            Key Y - game against a computer 
            Key N - game against a human
            """;

        public const string SHOW_THE_LOCATION_OF_THE_CONTROL_KEYS = """
            Location of the control keys:
            
            7   8   9

            4   5   6

            1   2   3            
            """;

        public const string PRESS_ANY_KEY_TO_CONTINUE = "Press any key to continue";

        public const string GAME_ON = "Game on";

        public const string NOW_TURN_PLAYER_1 = "Now it is the turn of Player 1";

        public const string NOW_TURN_PLAYER_2 = "Now it is the turn of Player 2";

        public const string PLAYER_1_IS_WINNER = "Congratulations! Player 1 is the winner!";

        public const string PLAYER_2_IS_WINNER = "Congratulations! Player 2 is the winner!";

        public const string GAME_HAS_ENDED_IN_DRAW = "Congratulations! The game has ended in a draw!";

        public const string PRESS_SPACE_BAR_OR_ESCAPE = "Press the space bar to continue the game, or Escape to quit!";
    }
}
