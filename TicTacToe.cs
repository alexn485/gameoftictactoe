﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Games
{
    public class TicTacToe
    {
        private Playfield _playfield;
        private int _moveCounter = 0;
        private bool _itIsGameWithComputer = false;
        private bool _iTIsItem_xTurnWhenMoveCounterIsOdd = false;
        private int _currentRowNumber;
        private int _currentColumnNumber;
        private bool _correctKeyIsNotPressed = true;
        private bool _quitGame = true;

        public TicTacToe(Playfield playfield)
        {
            _playfield = playfield;
        }

        public void RunGame()
        {
            while (_quitGame)
            {
                Console.CursorVisible = false;

                Console.ForegroundColor = ConsoleColor.Green;
                Console.BackgroundColor = ConsoleColor.White;

                ConsoleKeyInfo key = new ConsoleKeyInfo();

                MachineAsPlayer machineAsPlayer = new MachineAsPlayer(_playfield);

                while (_correctKeyIsNotPressed)
                {
                    Console.Clear();

                    Console.WriteLine(Message.WELCOME);
                    Console.WriteLine();

                    Console.WriteLine(Message.PRESS_THE_SPACE_BAR_TO_CONTINUE);

                    key = Console.ReadKey();

                    if (key.Key == ConsoleKey.Spacebar)
                    {
                        _correctKeyIsNotPressed = false;
                    }

                    Console.Clear();
                }

                _correctKeyIsNotPressed = true;

                while (_correctKeyIsNotPressed)
                {
                    Console.WriteLine(Message.CHOICE_PLAYER_1_WILL_PLAY_FOR);

                    key = Console.ReadKey();

                    switch (key.Key)
                    {
                        case ConsoleKey.X:
                            _correctKeyIsNotPressed = false;
                            break;

                        case ConsoleKey.O:
                            _iTIsItem_xTurnWhenMoveCounterIsOdd = true;
                            _correctKeyIsNotPressed = false;
                            break;
                    }

                    Console.Clear();
                }

                _correctKeyIsNotPressed = true;

                while (_correctKeyIsNotPressed)
                {
                    Console.WriteLine(Message.CHOICE_A_GAME_AGAINST_THE_COMPUTER);

                    key = Console.ReadKey();

                    switch (key.Key)
                    {
                        case ConsoleKey.Y:

                            _itIsGameWithComputer = true;

                            _correctKeyIsNotPressed = false;
                            break;

                        case ConsoleKey.N:
                            _correctKeyIsNotPressed = false;
                            break;
                    }

                    Console.Clear();
                }

                Console.WriteLine(Message.SHOW_THE_LOCATION_OF_THE_CONTROL_KEYS);
                Console.WriteLine();

                Console.WriteLine(Message.PRESS_ANY_KEY_TO_CONTINUE);

                key = Console.ReadKey();

                Console.Clear();

                Visualization visualization = new Visualization(_playfield);

                while (_playfield.NoneOfPlayersWon())
                {
                    _correctKeyIsNotPressed = true;

                    while (_correctKeyIsNotPressed)
                    {
                        Console.WriteLine(Message.GAME_ON);
                        Console.WriteLine();

                        if (ITIsFirstPlayerTurn())
                        {
                            Console.WriteLine(Message.NOW_TURN_PLAYER_1);
                        }
                        else
                        {
                            Console.WriteLine(Message.NOW_TURN_PLAYER_2);
                        }

                        Console.WriteLine();
                        Console.WriteLine();

                        visualization.ShowPlayField();

                        if (_itIsGameWithComputer && !ITIsFirstPlayerTurn())
                        {
                            key = machineAsPlayer.GetKeyPressedByMachineForPlayerNumber2(_iTIsItem_xTurnWhenMoveCounterIsOdd);
                        }
                        else
                        {
                            key = Console.ReadKey();
                        }

                        switch (key.Key)
                        {
                            case ConsoleKey.NumPad7:
                                _currentRowNumber = 0;
                                _currentColumnNumber = 0;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad8:
                                _currentRowNumber = 0;
                                _currentColumnNumber = 1;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad9:
                                _currentRowNumber = 0;
                                _currentColumnNumber = 2;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad4:
                                _currentRowNumber = 1;
                                _currentColumnNumber = 0;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad5:
                                _currentRowNumber = 1;
                                _currentColumnNumber = 1;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad6:
                                _currentRowNumber = 1;
                                _currentColumnNumber = 2;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad1:
                                _currentRowNumber = 2;
                                _currentColumnNumber = 0;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad2:
                                _currentRowNumber = 2;
                                _currentColumnNumber = 1;

                                _correctKeyIsNotPressed = false;
                                break;

                            case ConsoleKey.NumPad3:
                                _currentRowNumber = 2;
                                _currentColumnNumber = 2;

                                _correctKeyIsNotPressed = false;
                                break;
                        }

                        if (_playfield.GetItem(_currentRowNumber, _currentColumnNumber) != 0)
                        {
                            _correctKeyIsNotPressed = true;
                        }

                        Console.Clear();
                    }

                    SetItemToPlayfield(_currentRowNumber, _currentColumnNumber);

                    if (_playfield.NoneOfPlayersWon())
                    {
                        _moveCounter++;
                    }
                }

                _correctKeyIsNotPressed = true;

                while (_correctKeyIsNotPressed)
                {

                    visualization.ShowPlayField();
                    Console.WriteLine();
                    Console.WriteLine();

                    if (_playfield.AreAllCellsFilledIn())
                    {
                        Console.WriteLine(Message.GAME_HAS_ENDED_IN_DRAW);
                    }
                    else if (ITIsFirstPlayerTurn())
                    {
                        Console.WriteLine(Message.PLAYER_1_IS_WINNER);
                    }
                    else
                    {
                        Console.WriteLine(Message.PLAYER_2_IS_WINNER);
                    }

                    Console.WriteLine();
                    Console.WriteLine();
                    Console.WriteLine(Message.PRESS_SPACE_BAR_OR_ESCAPE);

                    key = Console.ReadKey();

                    switch (key.Key)
                    {
                        case ConsoleKey.Escape:
                            _correctKeyIsNotPressed = false;
                            _quitGame = false;
                            break;

                        case ConsoleKey.Spacebar:
                            _correctKeyIsNotPressed = false;
                            break;
                    }

                    Console.Clear();
                }
            }
        }
        private void SetItemToPlayfield(int rowNumber, int columnNumber)
        {
            if (ITIsFirstPlayerTurn() && _iTIsItem_xTurnWhenMoveCounterIsOdd)
            {
                _playfield.SetItem_o(rowNumber, columnNumber);
            }
            else if (ITIsFirstPlayerTurn() && !_iTIsItem_xTurnWhenMoveCounterIsOdd)
            {
                _playfield.SetItem_x(rowNumber, columnNumber);
            }
            else if (!ITIsFirstPlayerTurn() && _iTIsItem_xTurnWhenMoveCounterIsOdd)
            {
                _playfield.SetItem_x(rowNumber, columnNumber);
            }
            else if (!ITIsFirstPlayerTurn() && !_iTIsItem_xTurnWhenMoveCounterIsOdd)
            {
                _playfield.SetItem_o(rowNumber, columnNumber);
            }
        }

        private bool ITIsFirstPlayerTurn()
        {
            if (_moveCounter % 2 == 0)
            {
                return true;
            }

            return false;
        }
    }
}
