﻿namespace Games
{
    public class Playfield
    {
        private int _itemX = 1;
        public int ItemX => _itemX;

        private int _itemO = -1;
        public int ItemO => _itemO;

        private const int _emptyCell = 0; 

        private const int _maxNumberOfRows = 3;
        private const int _maxNumberColumns = 3;

        private int[,] _field = new int[_maxNumberOfRows, _maxNumberColumns];

        private int _rowNumber = 0;
        private int _columnNumber = 0;

        public Playfield()
        {
            for (int i = 0; i < _field.GetLength(0); i++)
            {
                for (int j = 0; j < _field.GetLength(1); j++)
                {
                    _field[i, j] = 0;
                }
            }
        }

        public void SetItem_x(int rowNumber, int columnNumber)
        {
            CheckRowNumberAndColumnNumberForPermissibleRange(rowNumber, columnNumber);

            _rowNumber = rowNumber;
            _columnNumber = columnNumber;

            _field[_rowNumber, _columnNumber] = _itemX;
        }

        public void SetItem_o(int rowNumber, int columnNumber)
        {
            CheckRowNumberAndColumnNumberForPermissibleRange(rowNumber, columnNumber);

            _rowNumber = rowNumber;
            _columnNumber = columnNumber;

            _field[_rowNumber, _columnNumber] = _itemO;
        }

        public bool NoneOfPlayersWon()
        {
            if (AreThere3IdenticalItemsInRow() || AreThere3IdenticalItemsInColumn() || AreThere3IdenticalItemsInDiagonallyGrowing() || AreThere3IdenticalItemsInDiagonallyDescending() || AreAllCellsFilledIn())
            {
                return false;
            }

            return true;
        }

        public bool AreThere3IdenticalItemsInRow()
        {
            int previousItem = 0;

            for (int i = 0; i < GetMaxLengthOfColumn(); i++)
            {
                for (int j = 1; j < GetMaxLengthOfRow(); j++)
                {
                    previousItem = GetItem(i, j - 1);

                    if (previousItem != GetItem(i, j) || GetItem(i, j) == 0)
                    {
                        break;
                    }

                    if (j == GetMaxLengthOfRow() - 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool AreThere3IdenticalItemsInColumn()
        {
            int previousItem = 0;

            for (int j = 0; j < GetMaxLengthOfRow(); j++)
            {
                for (int i = 1; i < GetMaxLengthOfColumn(); i++)
                {
                    previousItem = GetItem(i - 1, j);

                    if (previousItem != GetItem(i, j) || GetItem(i, j) == 0)
                    {
                        break;
                    }

                    if (i == GetMaxLengthOfColumn() - 1)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public bool AreThere3IdenticalItemsInDiagonallyGrowing()
        {
            int penultimateRow = GetMaxLengthOfColumn() - 2;

            int previousItem = 0;

            for (int i = penultimateRow, j = 1; i >= 0; i--, j++)
            {
                previousItem = GetItem(i + 1, j - 1);

                if (previousItem != GetItem(i, j) || GetItem(i, j) == 0)
                {
                    break;
                }

                if (i == 0)
                {
                    return true;
                }
            }

            return false;
        }

        public bool AreThere3IdenticalItemsInDiagonallyDescending()
        {
            int previousItem = 0;

            for (int i = 1, j = 1; i < GetMaxLengthOfColumn(); i++, j++)
            {
                previousItem = GetItem(i - 1, j - 1);

                if (previousItem != GetItem(i, j) || GetItem(i, j) == 0)
                {
                    break;
                }

                if (i == GetMaxLengthOfColumn() - 1)
                {
                    return true;
                }
            }

            return false;
        }

        public bool AreAllCellsFilledIn()
        {
            for (int i = 0; i < GetMaxLengthOfColumn(); i++)
            {
                for (int j = 0; j < GetMaxLengthOfRow(); j++)
                {
                    if (IsEmpty(i, j))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool IsFilledWithItem(int rowNumber, int columnNumber, int currentItem)
        {
            if (GetItem(rowNumber, columnNumber) == currentItem && currentItem != _emptyCell)
            {
                return true;
            }

            return false;
        }

        public bool IsEmpty(int rowNumber, int columnNumber)
        {
            if (GetItem(rowNumber, columnNumber) == _emptyCell)
            {
                return true;
            }

            return false;
        }



        public int GetItem(int rowNumber, int columnNumber)
        {
            CheckRowNumberAndColumnNumberForPermissibleRange(rowNumber, columnNumber);

            _rowNumber = rowNumber;
            _columnNumber = columnNumber;

            return _field[_rowNumber, _columnNumber];
        }

        public int GetMaxLengthOfRow()
        {
            return _maxNumberColumns;
        }

        public int GetMaxLengthOfColumn()
        {
            return _maxNumberOfRows;
        }

        public void CheckRowNumberAndColumnNumberForPermissibleRange(int rowNumber, int columnNumber)
        {
            if (rowNumber < 0 || rowNumber > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(rowNumber), "rowNumber can only be in the range from 0 to 2");
            }

            if (columnNumber < 0 || columnNumber > 2)
            {
                throw new ArgumentOutOfRangeException(nameof(columnNumber), "columnNumber can only be in the range from 0 to 2");
            }
        }
    }
}
