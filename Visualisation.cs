﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Games
{
    public class Visualization
    {
        private Playfield _playfield;
        
        private const char _visualShapeOfF_itemX = 'X';

        private const char _visualShapeOfFItemO = 'O';

        public Visualization(Playfield playfield)
        {
            _playfield = playfield;
        }

        public void ShowPlayField()
        {
            char currentShape = ' ';

            for (int i = 0; i < _playfield.GetMaxLengthOfRow(); i++)
            {
                for (int j = 0; j < _playfield.GetMaxLengthOfColumn(); j++)
                {
                    switch (_playfield.GetItem(i, j))
                    {
                        case 0:
                            currentShape = ' ';
                            break;

                        case 1:
                            currentShape = _visualShapeOfF_itemX;
                            break;

                        case -1:
                            currentShape = _visualShapeOfFItemO;
                            break;
                    }

                    Console.Write($"|{currentShape}|\t");
                }
                Console.WriteLine();
                Console.WriteLine();
            }
        }
    }
}
