﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Metrics;

namespace Games
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Playfield playfield = new Playfield();

            TicTacToe game = new TicTacToe(playfield);

            game.RunGame();
        }
    }
}

