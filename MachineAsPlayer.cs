﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Numerics;
using System.Reflection.PortableExecutable;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace Games

{
    public class MachineAsPlayer
    {
        private Playfield _playfield;
        private ConsoleKeyInfo _currentKey = new ConsoleKeyInfo();
        private List<ConsoleKeyInfo> keysThatMachineCanPressInCurrentTurn = new List<ConsoleKeyInfo>();

        private int _currentItem;

        private Random _random = new Random();

        public MachineAsPlayer(Playfield playfield)
        {
            _playfield = playfield;
        }

        public ConsoleKeyInfo GetKeyPressedByMachineForPlayerNumber2(bool iTIsItem_xTurnWhenMoveCounterIsOdd)
        {
            AddAllKeysToFieldKeysThatMachineCanPressInCurrentTurn(iTIsItem_xTurnWhenMoveCounterIsOdd);

            return GetRandomKeyFromFieldKeysThatMachineCanPressInCurrentTurn();    
        }
        private void AddAllKeysToFieldKeysThatMachineCanPressInCurrentTurn(bool iTIsItem_xTurnWhenMoveCounterIsOdd)
        {
            keysThatMachineCanPressInCurrentTurn.Clear();
            int numberOfPlayers = 2;

            for (int currentNumberOfPlacesInLine = 1; currentNumberOfPlacesInLine <= _playfield.GetMaxLengthOfColumn(); currentNumberOfPlacesInLine++)
            {
                for (int i = 0; i < numberOfPlayers; i++)
                {
                    if (iTIsItem_xTurnWhenMoveCounterIsOdd && i == 0)
                    {
                        _currentItem = _playfield.ItemX;
                    }
                    if (!iTIsItem_xTurnWhenMoveCounterIsOdd && i == 0)
                    {
                        _currentItem = _playfield.ItemO;
                    }

                    AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnAnyOfRows(currentNumberOfPlacesInLine);

                    AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnAnyOfColumns(currentNumberOfPlacesInLine);

                    AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnDiagonallyGrowing(currentNumberOfPlacesInLine);

                    AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnDiagonallyDescending(currentNumberOfPlacesInLine);

                    if (keysThatMachineCanPressInCurrentTurn.Any())
                    {
                        break;
                    }

                    if (_currentItem == _playfield.ItemX)
                    {
                        _currentItem = _playfield.ItemO;
                    }
                    else
                    {
                        _currentItem = _playfield.ItemX;
                    }
                }

                if (keysThatMachineCanPressInCurrentTurn.Any())
                {
                    break;
                }
            }

            keysThatMachineCanPressInCurrentTurn = keysThatMachineCanPressInCurrentTurn.Distinct().ToList();
        }
        private void AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnAnyOfRows(int currentNumberOfPlacesInLine)
        {
            List<ConsoleKeyInfo> keysThatMachineCanPressInCurrentTurnInSpecificRow = new List<ConsoleKeyInfo>();            
            int counterOfEmptySpacesFoundInCurrentRow = 0;

            for (int j = 0; j < _playfield.GetMaxLengthOfColumn(); j++)
            {
                counterOfEmptySpacesFoundInCurrentRow = 0;
                keysThatMachineCanPressInCurrentTurnInSpecificRow.Clear();

                for (int k = 0; k < _playfield.GetMaxLengthOfRow(); k++)
                {
                    if (!_playfield.IsFilledWithItem(j, k, _currentItem) && !_playfield.IsEmpty(j, k))
                    {
                        break;
                    }

                    if (_playfield.IsEmpty(j, k))
                    {
                        keysThatMachineCanPressInCurrentTurnInSpecificRow.Add(ConvertRowNumberAndColumnNumberToKey(j, k));

                        counterOfEmptySpacesFoundInCurrentRow++;
                    }

                    if (counterOfEmptySpacesFoundInCurrentRow > currentNumberOfPlacesInLine)
                    {
                        break;
                    }

                    if (k == (_playfield.GetMaxLengthOfRow() - 1) && counterOfEmptySpacesFoundInCurrentRow == currentNumberOfPlacesInLine)
                    {
                        keysThatMachineCanPressInCurrentTurn.AddRange(keysThatMachineCanPressInCurrentTurnInSpecificRow);
                    }
                }
            }
        }
        private void AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnAnyOfColumns(int currentNumberOfPlacesInLine)
        {
            List<ConsoleKeyInfo> keysThatMachineCanPressInCurrentTurnInSpecificColumn = new List<ConsoleKeyInfo>();    
            int counterOfEmptySpacesFoundInCurrentColumn = 0;

            for (int k = 0; k < _playfield.GetMaxLengthOfRow(); k++)
            {
                counterOfEmptySpacesFoundInCurrentColumn = 0;
                keysThatMachineCanPressInCurrentTurnInSpecificColumn.Clear();

                for (int j = 0; j < _playfield.GetMaxLengthOfColumn(); j++)
                {
                    if (!_playfield.IsFilledWithItem(j, k, _currentItem) && !_playfield.IsEmpty(j, k))
                    {
                        break;
                    }

                    if (_playfield.IsEmpty(j, k))
                    {
                        keysThatMachineCanPressInCurrentTurnInSpecificColumn.Add(ConvertRowNumberAndColumnNumberToKey(j, k));

                        counterOfEmptySpacesFoundInCurrentColumn++;
                    }

                    if (counterOfEmptySpacesFoundInCurrentColumn > currentNumberOfPlacesInLine)
                    {
                        break;
                    }

                    if (j == (_playfield.GetMaxLengthOfColumn() - 1) && counterOfEmptySpacesFoundInCurrentColumn == currentNumberOfPlacesInLine)
                    {
                        keysThatMachineCanPressInCurrentTurn.AddRange(keysThatMachineCanPressInCurrentTurnInSpecificColumn);
                    }
                }
            }
        }
        private void AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnDiagonallyGrowing(int currentNumberOfPlacesInLine)
        {
            List<ConsoleKeyInfo> keysThatMachineCanPressInCurrentTurnInDiagonallyGrowing = new List<ConsoleKeyInfo>();          
            int counterOfEmptySpacesFoundInCurrentDiagonallyGrowing = 0;

            for (int j = _playfield.GetMaxLengthOfColumn() - 1, k = 0; j >= 0; j--, k++)
            {
                if (!_playfield.IsFilledWithItem(j, k, _currentItem) && !_playfield.IsEmpty(j, k))
                {
                    break;
                }

                if (_playfield.IsEmpty(j, k))
                {
                    keysThatMachineCanPressInCurrentTurnInDiagonallyGrowing.Add(ConvertRowNumberAndColumnNumberToKey(j, k));

                    counterOfEmptySpacesFoundInCurrentDiagonallyGrowing++;
                }

                if (counterOfEmptySpacesFoundInCurrentDiagonallyGrowing > currentNumberOfPlacesInLine)
                {
                    break;
                }

                if (k == (_playfield.GetMaxLengthOfRow() - 1) && counterOfEmptySpacesFoundInCurrentDiagonallyGrowing == currentNumberOfPlacesInLine)
                {
                    keysThatMachineCanPressInCurrentTurn.AddRange(keysThatMachineCanPressInCurrentTurnInDiagonallyGrowing);
                }
            }
        }
        private void AddKeysToFieldKeysThatMachineCanPressInCurrentTurnToWinOnDiagonallyDescending(int currentNumberOfPlacesInLine)
        {
            List<ConsoleKeyInfo> keysThatMachineCanPressInCurrentTurnInDiagonallyDescending = new List<ConsoleKeyInfo>();
            int counterOfEmptySpacesFoundInCurrentDiagonallyDescending = 0;

            for (int j = 0, k = 0; j < _playfield.GetMaxLengthOfColumn(); j++, k++)
            {
                if (!_playfield.IsFilledWithItem(j, k, _currentItem) && !_playfield.IsEmpty(j, k))
                {
                    break;
                }

                if (_playfield.IsEmpty(j, k))
                {
                    keysThatMachineCanPressInCurrentTurnInDiagonallyDescending.Add(ConvertRowNumberAndColumnNumberToKey(j, k));

                    counterOfEmptySpacesFoundInCurrentDiagonallyDescending++;
                }

                if (counterOfEmptySpacesFoundInCurrentDiagonallyDescending > currentNumberOfPlacesInLine)
                {
                    break;
                }

                if (j == (_playfield.GetMaxLengthOfColumn() - 1) && counterOfEmptySpacesFoundInCurrentDiagonallyDescending == currentNumberOfPlacesInLine)
                {
                    keysThatMachineCanPressInCurrentTurn.AddRange(keysThatMachineCanPressInCurrentTurnInDiagonallyDescending);
                }
            }
        }
        public ConsoleKeyInfo ConvertRowNumberAndColumnNumberToKey(int rowNumber, int columnNumber)
        {
            _playfield.CheckRowNumberAndColumnNumberForPermissibleRange(rowNumber, columnNumber);

            switch (rowNumber, columnNumber)
                {
                    case (0, 0):
                        _currentKey = new ConsoleKeyInfo('7', ConsoleKey.NumPad7, false, false, false);
                        break;

                    case (0, 1):
                        _currentKey = new ConsoleKeyInfo('8', ConsoleKey.NumPad8, false, false, false);
                        break;

                    case (0, 2):
                        _currentKey = new ConsoleKeyInfo('9', ConsoleKey.NumPad9, false, false, false);
                        break;

                    case (1, 0):
                        _currentKey = new ConsoleKeyInfo('4', ConsoleKey.NumPad4, false, false, false);
                        break;

                    case (1, 1):
                        _currentKey = new ConsoleKeyInfo('5', ConsoleKey.NumPad5, false, false, false);
                        break;

                    case (1, 2):
                        _currentKey = new ConsoleKeyInfo('6', ConsoleKey.NumPad6, false, false, false);
                        break;

                    case (2, 0):
                        _currentKey = new ConsoleKeyInfo('1', ConsoleKey.NumPad1, false, false, false);
                        break;

                    case (2, 1):
                        _currentKey = new ConsoleKeyInfo('2', ConsoleKey.NumPad2, false, false, false);
                        break;

                    case (2, 2):
                        _currentKey = new ConsoleKeyInfo('3', ConsoleKey.NumPad3, false, false, false);
                        break;
                }

            return _currentKey;
        }
        public ConsoleKeyInfo GetRandomKeyFromFieldKeysThatMachineCanPressInCurrentTurn()
        {
            int randomIndex = _random.Next(0, keysThatMachineCanPressInCurrentTurn.Count);
           
            return keysThatMachineCanPressInCurrentTurn[randomIndex];
        }        
    }
}
